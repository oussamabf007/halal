import React from 'react';
import './main.css';
import Header from '../../shared/header';

const BlackList = () => {
	return (
		<div className='black-main'>
			<div>
				<Header text='black list' />
			</div>
			<div className='black-body'>
				<input type='text' placeholder='put the number here' />
				<p>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi autem
					ut incidunt corporis magnam hic, id possimus? Accusantium voluptatum
					in corporis corrupti a fugit necessitatibus. Eum, dicta in? Tenetur,
					asperiores.
				</p>
			</div>
		</div>
	);
};

export default BlackList;
