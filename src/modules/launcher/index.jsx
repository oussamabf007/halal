import React from 'react';
import LogoGreen from '../../assets/LogoGreen.png';
import './main.css';
import { Link } from 'react-router-dom';

export default function Launcher() {
	return (
		<div className='lancher_main'>
			<div className='lancher_logo'>
				<img src={LogoGreen} alt='logo' />
			</div>
			<div className='lancher_buttons_group'>
				<div className='lancher_button_item form-group'>
					<Link to='/sign'>
						<button className='btn-primary'>Sign in</button>
					</Link>
				</div>
				<div className='lancher_button_item form-group'>
					<Link to='/sign-up'>
						<button className='btn-primary'>Sign up</button>
					</Link>
				</div>
			</div>
		</div>
	);
}
