import React from 'react';
import { Link } from 'react-router-dom';
import LogoGreen from '../../assets/LogoGreen.png';
import './main.css';

export default function Sign() {
	return (
		<div className='lancher_main_signin'>
			<div className='container-logo'>
				<img src={LogoGreen} alt='logo' className='logo' />
				<p>هذا النص هو مثال لنص يمكن أن يستبدل</p>
			</div>

			<div className='signin-form'>
				<div className='sign_form_container'>
					<div className='form-group'>
						<input
							className='input-primary input-icon1'
							type='text'
							placeholder='اسم المستخدم'
						/>
					</div>

					<div className='form-group'>
						<input
							className='input-primary input-icon4'
							type='text'
							placeholder='كلمة المرور'
						/>
					</div>
					<p className='font-small'>هل نسيت كلمة المرور ؟</p>
				</div>
			</div>
			<div className='buttons-grp'>
				<div className='lancher_button_item form-group sign-up '>
					<button className='btn-primary'>Sign in</button>
				</div>
				<p className='p-transparent'>ليس لديك حساب؟</p>
				<div className='lancher_button_item form-group sign-up '>
					<Link to='/sign-up'>
						<button className='btn-transparent'>Sign up</button>
					</Link>
				</div>
			</div>
		</div>
	);
}
