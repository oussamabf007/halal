import React from 'react';
import backgroundSignin from '../../assets/backgroundSignin.png';
import logoLanding from '../../assets/logoLanding.png';
import { Link } from 'react-router-dom';
import './main.css';

const SignIn = () => {
	return (
		<div
			className='sigin_main'
			style={{ backgroundImage: `url('${backgroundSignin}')` }}
		>
			<div className='signin_logo'>
				<img src={logoLanding} alt='' />
			</div>
			<div className='signin_form'>
				<h1 className='signin_title'>sign in</h1>
				<div className='sign_form_container'>
					<div className='form-group'>
						<input
							className='input-primary input-icon-cellphone'
							type='text'
							placeholder='XXXX'
						/>
					</div>
					<div>
						<Link to='/sign'>
							<button className='btn-primary'>sign in</button>
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
};
export default SignIn;
