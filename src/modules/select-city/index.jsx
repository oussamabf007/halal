import React from 'react';
import './main.css';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';

const SelectCity = () => {
	let history = useHistory();
	return (
		<div className='select-main'>
			<div className='select-header'>
				<h3>selection</h3>
				<button onClick={() => history.goBack()} />
			</div>
			<div className='select-body'>
				<div className='tbtn-transparant'>
					<Link to='/choose-city'>
						<button className='tbtn'>city</button>
					</Link>
				</div>
				<div className='btn-widthhh'>
					<button className='btn-primary'>save selection</button>
				</div>
			</div>
		</div>
	);
};

export default SelectCity;
