import React from 'react';
import './main.css';
import Header from '../../shared/header';

function ContactUs() {
	return (
		<div className='contact-main'>
			<div>
				<Header text='contact us' />
			</div>
			<div className='grid'>
				<div className='input-row'>
					<label htmlFor=''>name</label>
					<input type='text' placeholder='oussama' />
				</div>
				<div className='input-row'>
					<label htmlFor=''>phone</label>
					<input type='text' placeholder='53623312' />
				</div>
				<div className='input-row'>
					<label htmlFor=''>email</label>
					<input type='text' placeholder='oussamabf007@gmail.com' />
				</div>
				<div className='input-row'>
					<label htmlFor=''>subject</label>
					<input type='text' />
				</div>
				<div className='input-row'>
					<label htmlFor=''>message</label>
					<input type='text' />
				</div>
			</div>
			<div className='buton-container'>
				<div className='buttonn-width'>
					<button className='btn-primary'>send</button>
				</div>
				<div>
					<h3>contact us on social media</h3>
					<div className='social-media-buttons'>
						<button className='btn-social1'></button>
						<button className='btn-social2'></button>
						<button className='btn-social3'></button>
						<button className='btn-social4'></button>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ContactUs;
