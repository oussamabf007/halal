import React from 'react';
import Header from '../../shared/header';
import './main.css';

const AddComment = () => {
	return (
		<div className='comment-main'>
			<Header text='Add comment' />
			<div className='comment-input'>
				<h3>Add comment</h3>
				<input type='text' />
			</div>
			<div className='btn-placinng'>
				<button className='btn-primary'>add comment</button>
			</div>
		</div>
	);
};

export default AddComment;
