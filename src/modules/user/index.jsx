import React, { useState } from 'react';
import Header from '../../shared/header';
import './main.css';
import omar from '../../assets/omar.png';
import ProductItem from '../../shared/product-item';

function User() {
	const [color, setColor] = useState('white');
	const [textColor, setTextColor] = useState('black');
	const [color1, setColor1] = useState('white');
	const [textColor1, setTextColor1] = useState('black');

	return (
		<div className='user-main'>
			<div>
				<Header />
			</div>
			<div className='grid-pro'>
				<div className='user-profiler'>
					<div>
						<img src={omar} alt='' />
					</div>
					<div>
						<h4>moch3al modhiri</h4>
						<p>******</p>
						<p>joined in 7 july 2020</p>
					</div>
					<div>
						<button className='btn-st'>follow</button>
					</div>
				</div>
				<div className='twobt'>
					<button className='smbt1' />
					<button className='smbt2' />
				</div>
				<div className='ltbt'>
					<button
						className='bbt1'
						style={{ background: color, color: textColor }}
						onClick={() => {
							setColor('green');
							setTextColor('white');
						}}
					>
						products
					</button>
					<button
						className='bbt2'
						style={{ background: color1, color: textColor1 }}
						onClick={() => {
							setColor1('green');
							setTextColor1('white');
						}}
					>
						rates
					</button>
				</div>
			</div>

			<div>
				<ProductItem />
				<ProductItem />
				<ProductItem />
				<ProductItem />
			</div>
		</div>
	);
}

export default User;
