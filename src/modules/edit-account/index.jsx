import React from 'react';
import './main.css';
import Header from '../../shared/header';

const EditAccount = () => {
	return (
		<div className='edit-main'>
			<Header text='edit account' />
			<div className='edit-content'>
				<div className='name-email'>
					<h3>oussama</h3>
					<p>oussamabf007@gmail.com</p>
				</div>
				<div className='inputs'>
					<div className='label-input'>
						<label>Name</label> <input type='text' placeholder='oussama' />
					</div>
					<div className='label-input'>
						<label>email</label>{' '}
						<input type='text' placeholder='oussamabf007@gmail.com' />
					</div>
					<div className='label-input'>
						<label>phone</label> <input type='text' placeholder='53623312' />
					</div>
					<div className='label-input-check'>
						<input type='checkbox' id='call-me' />{' '}
						<label htmlFor='call-me'>call me on whats up</label>
					</div>
				</div>
			</div>
			<div className='btn-placinn'>
				<button className='btn-primary'>Save</button>
			</div>
		</div>
	);
};

export default EditAccount;
