import React from 'react';
import './main.css';
import ProductItem from '../../shared/product-item';
import Footer from '../../shared/footer';

const MyAdverts = () => {
	return (
		<div className='adverts-main'>
			<div className='adverts-header'>
				<h3>my adverts</h3>
			</div>
			<div className='adverts-body'>
				<ProductItem />
				<ProductItem />
				<ProductItem />
			</div>
			<div>
				<Footer />
			</div>
		</div>
	);
};

export default MyAdverts;
