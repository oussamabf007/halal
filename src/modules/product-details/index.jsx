import React from 'react';
import './main.css';
import { Link } from 'react-router-dom';

function ProductDetails() {
	return (
		<div className='details-main'>
			<div className='details-head'>
				<button className='tt1'></button>
				<button className='tt2'></button>
			</div>
			<div>content</div>
			<div className='detail-footer'>
				<button className='nn1'></button>
				<Link to='/add-comment'>
					<button className='btn-primary nn2'>add comment</button>
				</Link>
			</div>
		</div>
	);
}

export default ProductDetails;
