import React from 'react';
import './main.css';
import Header from '../../shared/header';
import bank1 from '../../assets/bank-list/bank1.png';
import bank2 from '../../assets/bank-list/bank2.png';
import bank3 from '../../assets/bank-list/bank3.png';

const BankList = () => {
	return (
		<div className='bank-main'>
			<div>
				<Header text='our banks' />
			</div>
			<div className='bank-list'>
				<div className='bank-container'>
					<img src={bank1} alt='' />
					<p>Bank rajihi</p>
				</div>
				<div className='bank-container'>
					<img src={bank2} alt='' />
					<p>Bank al balad</p>
				</div>
				<div className='bank-container'>
					<img src={bank3} alt='' />
					<p>Bank Riadh</p>
				</div>
			</div>
		</div>
	);
};

export default BankList;
