import React from 'react';
import Header from '../../shared/header';
import './main.css';

const TermsUse = () => {
	return (
		<div className='terms-main'>
			<Header text='terms of use' />
			<div className='terms-text'>
				<p>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt sed
					maiores possimus suscipit quae illo deserunt exercitationem
					consequatur pariatur fugiat dolores dignissimos repellat, doloribus
					placeat praesentium quia? Dolorem, quidem ad.
				</p>
			</div>
		</div>
	);
};

export default TermsUse;
