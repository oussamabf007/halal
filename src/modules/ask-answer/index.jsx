import React from 'react';
import './main.css';
import Header from '../../shared/header';

function AskAnswer() {
	return (
		<div className='ask-main'>
			<Header text='ask and aswer' />
			<div className='ask-text'>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit ?</p>
				<p>
					Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium
					quasi dolorum hic neque omnis quo, et voluptatem esse obcaecati
					libero, facere soluta. Nesciunt quia, modi blanditiis error eos
					deserunt et.
				</p>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit ?</p>
				<p>
					Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium
					quasi dolorum hic neque omnis quo, et voluptatem esse obcaecati
					libero, facere soluta. Nesciunt quia, modi blanditiis error eos
					deserunt et.
				</p>
			</div>
		</div>
	);
}

export default AskAnswer;
