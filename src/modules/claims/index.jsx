import React from 'react';
import './main.css';
import Header from '../../shared/header';
import ClaimItem from '../../shared/claim-item';

const Claims = () => {
	return (
		<div className='claims-main'>
			<div>
				<Header text='claims' />
			</div>
			<div className='claims-body'>
				<ClaimItem date='2/10/2019' state='trated' code='#254595' />
				<ClaimItem date='2/10/2019' state='trated' code='#254595' />
				<ClaimItem date='2/10/2019' state='trated' code='#254595' />
			</div>
		</div>
	);
};

export default Claims;
