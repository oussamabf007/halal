import React from 'react';
import './main.css';
import Footer from '../../shared/footer';
import MessageItem from '../../shared/message-item';

const Messages = () => {
	return (
		<div className='messages-main'>
			<div className='messages-header'>
				<h3>messages</h3>
			</div>
			<div>
				<MessageItem />
				<MessageItem />
				<MessageItem />
				<MessageItem />
			</div>
			<div>
				<Footer />
			</div>
		</div>
	);
};

export default Messages;
