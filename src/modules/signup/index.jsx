import React from 'react';
import { Link } from 'react-router-dom';
import LogoGreen from '../../assets/LogoGreen.png';
import './main.css';

const SignUp = () => {
	return (
		<div className='lancher_main_signin'>
			<div className='container-logo'>
				<img src={LogoGreen} alt='logo' className='logo' />
				<p>هذا النص هو مثال لنص يمكن أن يستبدل</p>
			</div>

			<div className='signin-form'>
				<div className='sign_form_container'>
					<div className='form-group'>
						<input
							className='input-primary input-icon1'
							type='text'
							placeholder='اسم المستخدم'
						/>
					</div>
					<div className='form-group'>
						<input
							className='input-primary input-icon2'
							type='text'
							placeholder='XXXX'
						/>
					</div>
					<div className='form-group'>
						<input
							className='input-primary input-icon3'
							type='text'
							placeholder='البريد الإلكتروني'
						/>
					</div>
					<div className='form-group'>
						<input
							className='input-primary input-icon4'
							type='text'
							placeholder='كلمة المرور'
						/>
					</div>
				</div>
			</div>
			<div className='lancher_button_item form-group sign-up '>
				<button className='btn-primary'>Sign up</button>
			</div>
			<p className='link-container'>
				لديك حساب الفعل؟
				<Link to='/sign-in' className='link'>
					تسجيل الدخول{' '}
				</Link>
			</p>
		</div>
	);
};
export default SignUp;
