import React from 'react';
import './main.css';
import moch3al from '../../assets/moch3al.png';
import Footer from '../../shared/footer';
import { Link } from 'react-router-dom';

const Profile = () => {
	return (
		<div className='profile-main'>
			<div className='profile-head'>
				<h4>my profile</h4>
				<Link to='/edit-account'>
					<button className='btn-edit'>edit</button>
				</Link>
			</div>
			<div className='profile-imge'>
				<img src={moch3al} alt='' />
				<div>
					<h2>moch3al morchid mtihri</h2>
					<p>mishaalmreshid2020@gmail.com</p>
				</div>
			</div>
			<div className='profile-body'>
				<h3>my applications</h3>
				<div className='tbtn-transparant'>
					<button className='tbtn1'>followers</button>
				</div>
				<div className='tbtn-transparant'>
					<Link to='/claims'>
						<button className='tbtn1'>claims</button>
					</Link>
				</div>
				<h3>support</h3>
				<div className='tbtn-transparant'>
					<Link to='/contact'>
						<button className='tbtn1'>contact us</button>
					</Link>
				</div>
				<div className='tbtn-transparant'>
					<Link to='/black'>
						<button className='tbtn1'>black list</button>
					</Link>
				</div>
				<div className='tbtn-transparant'>
					<Link to='/ask'>
						<button className='tbtn1'>ask and answer</button>
					</Link>
				</div>
				<div className='tbtn-transparant'>
					<Link to='/terms'>
						<button className='tbtn1'>terms of use</button>
					</Link>
				</div>
				<div className='tbtn-transparant'>
					<Link to='/bank'>
						<button className='tbtn2'>our banks</button>
					</Link>
				</div>
				<div className='tbtn-transparant'>
					<Link to='/sign-in'>
						<button className='tbtn3'>log out</button>
					</Link>
				</div>
			</div>
			<div>
				<Footer />
			</div>
		</div>
	);
};

export default Profile;
