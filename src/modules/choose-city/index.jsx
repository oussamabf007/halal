import React from 'react';
import './main.css';
import { useHistory } from 'react-router-dom';

const ChooseCity = () => {
	let history = useHistory();
	return (
		<div className='choose-main'>
			<div className='header-bt-container'>
				<button className='btnn1' onClick={() => history.goBack()}></button>
				<button className='btnn2' onClick={() => history.goBack()}></button>
			</div>
			<div className='choose-body-main'>
				<div className='inp-labeel'>
					<h4>city</h4>
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city1'>city1</label>
					<input type='checkbox' id='city1' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city2'>city2</label>
					<input type='checkbox' id='city2' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city3'>city3</label>
					<input type='checkbox' id='city3' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city4'>city4</label>
					<input type='checkbox' id='city4' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city5'>city5</label>
					<input type='checkbox' id='city5' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city6'>city6</label>
					<input type='checkbox' id='city6' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city7'>city7</label>
					<input type='checkbox' id='city7' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city8'>city8</label>
					<input type='checkbox' id='city8' />{' '}
				</div>
				<div className='inp-labeel'>
					<label htmlFor='city9'>city9</label>
					<input type='checkbox' id='city9' />{' '}
				</div>
			</div>
		</div>
	);
};

export default ChooseCity;
