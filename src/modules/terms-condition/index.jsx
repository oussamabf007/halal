import React from 'react';
import Header from '../../shared/header';
import './main.css';

const TermsCondition = () => {
	return (
		<div className='condition-main'>
			<Header text='conditions' />
			<div className='terms-text'>
				<p>
					Lorem, ipsum dolor sit amet consectetur adipisicing elit. Distinctio
					ullam in explicabo quisquam porro fugit accusamus quaerat rem
					assumenda culpa earum dolore dolor excepturi minus ut, quae ad hic?
					Eum!
				</p>
				<input type='checkbox' id='chekk' />
				<label htmlFor='chekk'>i have read the terms and i agree</label>
			</div>
			<div className='btnbtn'>
				<button className='btn-primary' disabled='true'>
					Agree
				</button>
			</div>
		</div>
	);
};

export default TermsCondition;
