import React from 'react';
import './main.css';

const CreateAccount = () => {
	return (
		<div className='create-main'>
			<div>
				<h1>Create Account</h1>

				<div className='form-grp'>
					<div className='form-group'>
						<input
							className='input-primary input-icon1'
							type='text'
							placeholder='اسم المستخدم'
						/>
					</div>
					<div className='form-group'>
						<input
							className='input-primary input-icon2'
							type='text'
							placeholder='XXXX'
						/>
					</div>
					<div className='form-group'>
						<input
							className='input-primary input-icon3'
							type='text'
							placeholder='البريد الإلكتروني'
						/>
					</div>
				</div>
			</div>
			<div className='btn-alone'>
				<button className='btn-primary'>Create</button>
			</div>
		</div>
	);
};
export default CreateAccount;
