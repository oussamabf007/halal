import React from 'react';
import './main.css';

const ClaimItem = props => {
	return (
		<div className='claims-item'>
			<div className='left-claim'>
				<p className='text-date'>{props.date}</p>
				<p className='text-state'>{props.state}</p>
			</div>
			<div>
				<h3>{props.code}</h3>
			</div>
		</div>
	);
};

export default ClaimItem;
