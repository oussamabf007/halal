import React from 'react';
import './main.css';

function MessageItem() {
	return (
		<div className='message-item-main'>
			<div className='message-item-icon'>
				<button />
			</div>
			<div className='message-item-left'>
				<p style={{ fontWeight: 'bold' }}>name</p>
				<p>message content</p>
			</div>
			<div className='message-item-right'>
				<p>8:42 Am</p>
			</div>
		</div>
	);
}

export default MessageItem;
