import React from 'react';
import { useHistory } from 'react-router-dom';

const Header = props => {
	let history = useHistory();
	return (
		<div className='header'>
			<div className='text-center'>
				<h3>{props.text}</h3>
			</div>
			<button className='btn-logo' onClick={() => history.goBack()} />
		</div>
	);
};

export default Header;
