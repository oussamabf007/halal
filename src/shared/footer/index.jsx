import React from 'react';
import './main.css';
import { Link } from 'react-router-dom';

const Footer = () => {
	return (
		<div className='footer-main'>
			<Link to='/profile'>
				<button className='footer-btn1' />
			</Link>
			<Link to='/messages'>
				<button className='footer-btn2' />
			</Link>
			<Link to='/add-advert'>
				<button className='footer-btn3'></button>
			</Link>
			<Link to='/my-adverts'>
				<button className='footer-btn4' />
			</Link>
			<Link to='/profile'>
				<button className='footer-btn5' />
			</Link>
		</div>
	);
};

export default Footer;
