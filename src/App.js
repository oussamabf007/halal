import './App.css';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
} from 'react-router-dom';
import SignIn from './modules/signin';
import SignUp from './modules/signup';
import Launcher from './modules/launcher';
import Sign from './modules/sign';
import CreateAccount from './modules/create-account';
import EditAccount from './modules/edit-account';
import AddComment from './modules/add-comment';
import TermsUse from './modules/terms-use';
import AskAnswer from './modules/ask-answer';
import TermsCondition from './modules/terms-condition';
import ContactUs from './modules/contact-us';
import BlackList from './modules/black-list';
import BankList from './modules/bank-list';
import SelectCity from './modules/select-city';
import ChooseCity from './modules/choose-city';
import Claims from './modules/claims';
import MyAdverts from './modules/my-adverts';
import Messages from './modules/messages';
import ProductDetails from './modules/product-details';
import Rate from './modules/rate';
import AddAdvert from './modules/add-advert';
import EditAdvert from './modules/edit-advert';
import User from './modules/user';
import Profile from './modules/profile';
import i18n from './translate/i18n';

function App() {
	return (
		<div className='App'>
			<Router>
				<Switch>
					<Route path='/launcher'>
						<Launcher />
					</Route>
					<Route path='/sign-up'>
						<SignUp />
					</Route>
					<Route path='/sign-in'>
						<SignIn />
					</Route>
					<Route path='/sign'>
						<Sign />
					</Route>
					<Route path='/create-account'>
						<CreateAccount />
					</Route>
					<Route path='/edit-account'>
						<EditAccount />
					</Route>
					<Route path='/add-comment'>
						<AddComment />
					</Route>
					<Route path='/terms'>
						<TermsUse />
					</Route>
					<Route path='/ask'>
						<AskAnswer />
					</Route>
					<Route path='/conditions'>
						<TermsCondition />
					</Route>
					<Route path='/contact'>
						<ContactUs />
					</Route>
					<Route path='/black'>
						<BlackList />
					</Route>
					<Route path='/bank'>
						<BankList />
					</Route>
					<Route path='/select-city'>
						<SelectCity />
					</Route>
					<Route path='/choose-city'>
						<ChooseCity />
					</Route>
					<Route path='/claims'>
						<Claims />
					</Route>
					<Route path='/my-adverts'>
						<MyAdverts />
					</Route>
					<Route path='/messages'>
						<Messages />
					</Route>
					<Route path='/product-details'>
						<ProductDetails />
					</Route>
					<Route path='/rate'>
						<Rate />
					</Route>
					<Route path='/add-advert'>
						<AddAdvert />
					</Route>
					<Route path='/edit-advert'>
						<EditAdvert />
					</Route>
					<Route path='/user'>
						<User />
					</Route>
					<Route path='/profile'>
						<Profile />
					</Route>
					<Redirect to='/sign-in' />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
