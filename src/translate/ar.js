const resource = {
	ar: {
		translation: {
			Login: 'تسجيل الدخول',
			Email: 'البريد الإلكتروني',
			Password: 'كلمة  السر',
			'Do not have an account?': 'لا تملك حساب؟',
			'Create an account': 'انشئ حساب',
			Reset: 'إعادة تعيين',
			Register: 'تسجيل',
			Name: 'اسم',
			Phone: 'الهاتف',
			'call me on whats up ': 'هاتفني على الواطس اب',
			Save: 'تسجيل',
			'My Adverts': 'إعلاناتي',
			'edit item': 'تعديل المنتج',
			'3alouch holandi': 'خروف هولندي',
			Messages: 'الرسائل',
			'My profile': 'حسابي',
			'My application': 'تطبيقي',
			followers: 'متابعين',
			claims: 'الشكاوي',
			Support: 'الدعم',
			'Contact us': 'تواصل معنا',
			'black list': 'القائمة السوداء',
			'ask and answer': 'أسئلة وإجابات',
			'terms of use': 'معاهدة استخدام الخدمة',
			'bank list': 'حسابتنا البنكية',
		},
	},
};

export default resource;
